# Set the base image to Ubuntu
FROM python:slim-stretch

# Set variables for project name, and where to place files in container.
ENV PROJECT=auto-panorama
ENV CONTAINER_HOME=/opt
ENV CONTAINER_PROJECT=$CONTAINER_HOME/$PROJECT

# Create application subdirectories
WORKDIR $CONTAINER_HOME
RUN mkdir logs

# Copy application source code to $CONTAINER_PROJECT
COPY . $CONTAINER_PROJECT

RUN apt-get update && apt-get install -y -q ffmpeg gettext memcached libpq-dev

# Install Python dependencies
RUN pip3 install -r $CONTAINER_PROJECT/src/requirements.txt

# Copy and set entrypoint
WORKDIR $CONTAINER_PROJECT

COPY entrypoint.sh /
ENTRYPOINT ["sh", "/entrypoint.sh"]