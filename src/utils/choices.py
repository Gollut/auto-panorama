from utils.choice_enum import ChoiceEnum
from django.utils.translation import gettext_lazy as _


class MaturityRating(ChoiceEnum):
    G = 'G'
    PG = 'PG'
    PG13 = 'PG13'
    R = 'R'
    NC17 = 'NC17'

    @classmethod
    def choices(cls):
        return (
            (cls.G, _('General Audiences (G)')),
            (cls.PG, _('Parental Guidance Suggested (PG)')),
            (cls.PG13, _('Parents Strongly Cautioned (PG13)')),
            (cls.R, _('Restricted (R)')),
            (cls.NC17, _('Adults Only (NC17)')),
        )
