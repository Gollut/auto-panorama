from pprint import pformat, pprint

from django.conf import settings
from django.test import override_settings
from django.urls import reverse
from django.utils.functional import cached_property
from rest_framework import status
from rest_framework.test import APITestCase
from utils.tests.factories import MainFactory, PrimitiveFactory, WatchingFactory


class TestCaseMeta(type):
    def __new__(mcs, name, bases, attrs):
        result = super().__new__(mcs, name, bases, attrs)
        return override_settings(MEDIA_ROOT=settings.BASE_DIR.joinpath('test_media'))(result)


class BaseTestCase(APITestCase, metaclass=TestCaseMeta):
    VIEW = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.primitive_factory = PrimitiveFactory()
        cls.main_factory = MainFactory(cls.primitive_factory)
        cls.watching_factory = WatchingFactory(cls.primitive_factory)

    def assertStatus(self, status_code, response):
        message = pformat(response.json()) if response.content else None
        self.assertEqual(status_code, response.status_code, message)

    def assertLen(self, sizable, expected_len):
        self.assertEqual(len(sizable), expected_len)

    def assertLenPaginatedResponse(self, response, expected_len):
        self.assertLen(response.json()['results'], expected_len)

    def assert200(self, response):
        self.assertStatus(status.HTTP_200_OK, response)

    def assert201(self, response):
        self.assertStatus(status.HTTP_201_CREATED, response)

    def assert202(self, response):
        self.assertStatus(status.HTTP_202_ACCEPTED, response)

    def assert400(self, response):
        self.assertStatus(status.HTTP_400_BAD_REQUEST, response)

    def get_reverse_kwargs(self):
        return {}

    @cached_property
    def url(self):
        return reverse(self.VIEW, kwargs=self.get_reverse_kwargs())

    def print(self, obj):
        pprint(obj)

    def rprint(self, r):
        self.print(r.json())


class AuthAPITestCase(BaseTestCase):
    def setUp(self):
        self.client.force_authenticate(user=self.user)

    @cached_property
    def user(self):
        return self.main_factory.get_user()


class ProfileAPITestCase(AuthAPITestCase):
    def get_profile(self):
        return self.main_factory.get_profile(user=self.user)

    @cached_property
    def profile(self):
        return self.get_profile()

    def setUp(self):
        self.client.force_authenticate(user=self.profile.user)
