from unittest.mock import patch, MagicMock


def patch_storage(func):
    def side_effect(name, *args, **kwargs):
        return name

    return patch('django.core.files.storage.FileSystemStorage.save', MagicMock(side_effect=side_effect))(func)
