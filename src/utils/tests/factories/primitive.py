import math
from random import choices, randint
from string import digits, ascii_letters, ascii_lowercase
from datetime import date, timedelta
from uuid import uuid4, UUID

from django.conf import settings
from django.core.files import File

digits_letters = digits + ascii_letters


class PrimitiveFactory:
    def get_string(self, length: int = 8, *, alphabet=digits_letters) -> str:
        return ''.join(choices(alphabet, k=length))

    def get_text(self, words: int = 10) -> str:
        result = ' '.join(self.get_string() for _ in range(words))
        return result.title()

    def get_int(self, left=0, right=100) -> int:
        return randint(left, right)

    def get_title(self, length: int = 16) -> str:
        return self.get_string(length, alphabet=ascii_lowercase).capitalize()

    def _days_in_month(self, m_index) -> int:
        return 28 + (m_index + math.floor(m_index / 8)) % 2 + 2 % m_index + 2 * math.floor(1 / m_index)

    def get_date(self, day=None, month=None, year=None) -> date:
        if month is None:
            month = self.get_int(1, 12)

        if day is None:
            day = self.get_int(1, self._days_in_month(month))

        if year is None:
            year = self.get_int(1990, 2018)

        return date(year, month, day)

    def get_uuid(self) -> UUID:
        return uuid4()

    def get_duration(self) -> timedelta:
        return timedelta(hours=1, minutes=20)

    def get_image(self):
        name = 'image.jpg'
        path = settings.BASE_DIR.joinpath('utils', 'tests', 'samples', name)
        return open(path, 'rb')

    def get_image_file(self) -> File:
        return File(self.get_image(), 'image.jpg')
