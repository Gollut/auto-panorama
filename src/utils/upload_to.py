from uuid import uuid4

from django.utils.deconstruct import deconstructible


@deconstructible
class UploadToFactory:
    extensions = ('tar.gz',)

    def __init__(self, path=None):
        self.path = path

    def get_extension(self, filename):
        for ext in self.extensions:
            if filename.endswith(ext):
                return ext
        return filename.rsplit('.', 1)[-1]

    def __call__(self, instance, filename):
        path = self.get_path(instance)
        ext = self.get_extension(filename)

        return '{path}/{filename}.{extension}'.format(path=path, filename=uuid4().hex, extension=ext)

    def get_path(self, instance):
        assert self.path is not None
        return self.path
