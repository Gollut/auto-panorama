# noinspection PyUnresolvedReferences
class FilterQuerysetMixin:
    filter_kwargs = {}

    def filter_queryset(self, queryset):
        filter_kwargs = {
            lookup: self.kwargs[kwarg]
            for lookup, kwarg in
            self.filter_kwargs.items()
        }
        return super().filter_queryset(queryset).filter(**filter_kwargs)
