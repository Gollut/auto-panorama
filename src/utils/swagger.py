from django.utils.decorators import method_decorator
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from drf_yasg.views import get_schema_view

schema_view = get_schema_view(
    openapi.Info(
        title='Auto-panorama API',
        default_version='v1',
    ),
    public=True,
    permission_classes=(),
    authentication_classes=(),
)


def schema_method_decorator(name, **kwargs):
    def wrapper(cls):
        return method_decorator(decorator=swagger_auto_schema(**kwargs), name=name)(cls)

    return wrapper
