from smtplib import SMTPException

from celery import shared_task
from django.core.mail import send_mail


@shared_task(
    autoretry_for=(SMTPException,),
    retry_jitter=True,
    retry_backoff=4,
    max_retries=3,
)
def send_email_task(subject, message, from_email, recipient_list, **kwargs):
    send_mail(subject, message, from_email, recipient_list, **kwargs)
