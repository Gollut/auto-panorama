from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode


class ConfirmLinkMixin:
    frontend_location = None

    def get_confirm_link(self, user):
        assert self.frontend_location is not None
        uid = urlsafe_base64_encode(force_bytes(user.pk))
        token = self.token_generator.make_token(user)

        location = self.frontend_location.format(uid=uid, token=token)
        link = self.request.build_absolute_uri(location)
        return link
