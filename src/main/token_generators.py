from django.conf import settings
from django.utils.crypto import constant_time_compare, salted_hmac


class BaseTokenGenerator:
    key_salt = None
    secret = settings.SECRET_KEY

    def make_token(self, user):
        assert self.key_salt is not None

        hash_string = salted_hmac(
            self.key_salt,
            self._make_hash_value(user),
            secret=self.secret
        ).hexdigest()[::2]

        return hash_string

    def check_token(self, user, token):
        return constant_time_compare(self.make_token(user), token)

    def _make_hash_value(self, user):
        return str(user.pk) + user.password


class ConfirmTokenGenerator(BaseTokenGenerator):
    key_salt = '97333dff70654c22bb56ab082626ad28'


class PasswordResetTokenGenerator(BaseTokenGenerator):
    key_salt = '2dab868d16c0481faa60876589a244c5'
