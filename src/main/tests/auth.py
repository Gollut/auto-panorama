from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from main.token_generators import ConfirmTokenGenerator, PasswordResetTokenGenerator
from utils.tests.base_classes import BaseTestCase

__all__ = [
    'TestSignUpView',
    'TestSignUpConfirmView',
    'TestSignInView',
    'TestPasswordRestoreView',
    'TestPasswordRestoreConfirmView'
]


class TestSignUpView(BaseTestCase):
    VIEW = 'main:sign_up'

    def test_post(self):
        password = self.main_factory.get_password()
        data = {
            'email': self.main_factory.get_email(),
            'password': password,
            'password_confirm': password,
        }

        r = self.client.post(self.url, data)

        self.assert201(r)


class TestSignUpConfirmView(BaseTestCase):
    VIEW = 'main:sign_up_confirm'
    token_generator = ConfirmTokenGenerator()

    def setUp(self):
        self.user = self.main_factory.get_user()

    def get_reverse_kwargs(self):
        return {
            'uid': urlsafe_base64_encode(force_bytes(self.user.pk)),
        }

    def test_post(self):
        data = {
            'token': self.token_generator.make_token(self.user)
        }
        r = self.client.post(self.url, data)
        self.assert200(r)


class TestSignInView(BaseTestCase):
    VIEW = 'main:sign_in'

    def test_post(self):
        password = self.main_factory.get_password()
        user = self.main_factory.get_user(password=password)
        data = {
            'email': user.email,
            'password': password,
        }

        r = self.client.post(self.url, data)
        self.assert200(r)


class TestPasswordRestoreView(BaseTestCase):
    VIEW = 'main:restore_password'

    def setUp(self):
        self.user = self.main_factory.get_user()

    def test_post(self):
        data = {
            'email': self.user.email,
        }
        r = self.client.post(self.url, data)
        self.assert202(r)


class TestPasswordRestoreConfirmView(BaseTestCase):
    VIEW = 'main:restore_password_confirm'
    token_generator = PasswordResetTokenGenerator()

    def setUp(self):
        self.user = self.main_factory.get_user(is_confirm=True)

    def get_reverse_kwargs(self):
        return {'uid': urlsafe_base64_encode(force_bytes(self.user.pk))}

    def test_post(self):
        password = self.main_factory.get_password()

        data = {
            'token': self.token_generator.make_token(self.user),
            'password': password,
            'password_confirm': password,
        }

        r = self.client.post(self.url, data)
        self.assert200(r)
