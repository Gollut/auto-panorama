from django.contrib.auth import authenticate
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from main.models import Profile, User
from main.validators import validate_password_pair, validate_user


class PasswordField(serializers.CharField):
    def __init__(self, **kwargs):
        kwargs.setdefault('write_only', True)
        kwargs.setdefault('min_length', 8)
        kwargs.setdefault('max_length', 128)
        super().__init__(**kwargs)


class SignUpSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(source='user.email')
    password = PasswordField(source='user.password')
    password_confirm = PasswordField(source='user.password_confirm')

    class Meta:
        model = Profile
        fields = ('email', 'password', 'password_confirm')

    def validate(self, attrs):
        user_attrs = attrs.pop('user')
        confirm_password = user_attrs.pop('password_confirm')
        password = user_attrs['password']

        validate_password_pair(password, confirm_password)
        validate_user(user_attrs)

        return {'user': user_attrs, **attrs}

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user = self._create_user(user_data)
        return Profile.objects.create(user=user, **validated_data)

    def _create_user(self, validated_data):
        return User.objects.create_user(**validated_data)


class SignUpConfirmSerializer(serializers.Serializer):
    token = serializers.CharField()

    def validate_token(self, token):
        token_generator = self.context['generator']
        if not token_generator.check_token(self.instance, token):
            raise ValidationError(_('Invalid token'))
        return token


class SignInSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = PasswordField()

    def validate(self, attrs):
        user = authenticate(**attrs)
        if user is None:
            raise ValidationError(_('Invalid credentials'), 'invalid_credentials')
        return {'user': user}


class PasswordRestoreSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate(self, attrs):
        email = User.objects.normalize_email(attrs['email'])
        try:
            user = User.objects.get(is_confirm=True, is_active=True, email=email)
        except User.DoesNotExist:
            raise ValidationError({'email': _('User with such email does not exist')})

        return {'user': user}


class PasswordRestoreConfirmSerializer(serializers.Serializer):
    token = serializers.CharField()
    password = PasswordField()
    password_confirm = PasswordField()

    def validate_token(self, token):
        token_generator = self.context['generator']
        if not token_generator.check_token(self.instance, token):
            raise ValidationError(_('Invalid token'))
        return token

    def validate(self, attrs):
        password, confirm_password = attrs['password'], attrs['password_confirm']
        validate_password_pair(password, confirm_password)
        return {'password': password}

    def update(self, instance, validated_data):
        password = validated_data['password']
        instance.set_password(password)
        instance.save()
        return instance
