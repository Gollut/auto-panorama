from django.conf import settings
from django.db import transaction
from django.utils.decorators import method_decorator
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import gettext_lazy as _
from rest_framework import status
from rest_framework.exceptions import NotFound, ParseError
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from main import serializers
from main.email_managers import SignUpEmailManager, RestorePasswordEmailManager
from main.mixins import ConfirmLinkMixin
from main.models import User
from main.token_generators import ConfirmTokenGenerator, PasswordResetTokenGenerator
from utils.swagger import schema_method_decorator as smd


@method_decorator(transaction.atomic, 'post')
class SignUpView(ConfirmLinkMixin, CreateAPIView):
    serializer_class = serializers.SignUpSerializer

    token_generator = ConfirmTokenGenerator()
    email_manager = SignUpEmailManager()

    frontend_location = settings.FRONTEND_CONFIG['SIGN_UP_CONFIRM_URL']

    def perform_create(self, serializer):
        profile = serializer.save()
        link = self.get_confirm_link(profile.user)
        self.email_manager.send_email(profile.email, link)


@smd('post', request_body=serializers.SignUpConfirmSerializer)
class SignUpConfirmView(APIView):
    token_generator = ConfirmTokenGenerator()

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        user = self.get_user()
        serializer = serializers.SignUpConfirmSerializer(
            user, request.data,
            context={'generator': self.token_generator}
        )
        serializer.is_valid(raise_exception=True)
        user.is_confirm = True
        user.save()
        return Response(status=status.HTTP_200_OK)

    def get_user(self):
        try:
            uid = urlsafe_base64_decode(self.kwargs['uid']).decode()
            user = User.objects.get(pk=uid, is_confirm=False, is_active=True)
        except User.DoesNotExist:
            raise NotFound(_('User does not exists or already confirmed'))
        except (TypeError, ValueError, OverflowError):
            raise ParseError(_('Decode error'))

        return user


@smd('post', request_body=serializers.SignInSerializer)
class SignInView(APIView):

    def post(self, request, *args, **kwargs):
        serializer = serializers.SignInSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.validated_data['user']
            return Response({'token': user.auth_token.key}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


@smd('post', request_body=serializers.PasswordRestoreSerializer)
class PasswordRestoreView(ConfirmLinkMixin, APIView):
    authentication_classes = ()
    token_generator = PasswordResetTokenGenerator()
    email_manager = RestorePasswordEmailManager()
    frontend_location = settings.FRONTEND_CONFIG['PASSWORD_RESTORE_CONFIRM_URL']

    def post(self, request, *args, **kwargs):
        serializer = serializers.PasswordRestoreSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.validated_data['user']
            link = self.get_confirm_link(user)
            self.email_manager.send_email(user.email, link)
        return Response(status=status.HTTP_202_ACCEPTED)


@smd('post', request_body=serializers.PasswordRestoreConfirmSerializer)
class PasswordRestoreConfirmView(APIView):
    token_generator = PasswordResetTokenGenerator()

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        user = self.get_user()
        serializer = serializers.PasswordRestoreConfirmSerializer(
            user, request.data,
            context={'generator': self.token_generator}
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status.HTTP_200_OK)

    def get_user(self):
        try:
            uid = urlsafe_base64_decode(self.kwargs['uid']).decode()
            user = User.objects.get(pk=uid, is_confirm=True, is_active=True)
        except User.DoesNotExist:
            raise NotFound(_('User does not exists'))
        except (TypeError, ValueError, OverflowError):
            raise ParseError(_('Decode error'))

        return user
