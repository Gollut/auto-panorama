# noinspection PyUnresolvedReferences
from .base import *

DEBUG = True
ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'auto_panorama',
        'USER': 'auto_panorama_user',
        'PASSWORD': 'password123',
        'HOST': 'localhost',
        'PORT': '',
    }
}

# Email settings
EMAIL_FROM = 'no-reply@autopanorama.com'
EMAIL_HOST = 'smtp.mailgun.org'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_PASSWORD = 'a89432d8dedd5c3a7e20ffa3996c317a-2d27312c-dbbb5a3a'
EMAIL_HOST_USER = 'postmaster@7polis.ru'

# Celery settings
BROKER_HOST = 'localhost'
BROKER_PORT = 5672
BROKER_USER = 'auto_panorama_user'
BROKER_PASSWORD = 'password123'
CELERY_BROKER_URL = f'amqp://{BROKER_USER}:{BROKER_PASSWORD}@{BROKER_HOST}:{BROKER_PORT}/'
CELERY_TASK_IGNORE_RESULT = True
