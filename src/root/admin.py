from django.contrib.admin import AdminSite as BaseAdminSite
from django.contrib.admin.apps import AdminConfig as BaseAdminConfig
from django.utils.translation import gettext_lazy as _


class AdminSite(BaseAdminSite):
    site_header = _('AutoPanorama')
    site_title = _('AutoPanorama. Site admin panel')


class AdminConfig(BaseAdminConfig):
    default_site = 'root.admin.AdminSite'
