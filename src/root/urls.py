from django.contrib import admin
from django.urls import path, include

from utils.swagger import schema_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('main/', include('main.urls')),
    path('watching/', include('watching.urls')),
    path('docs/', schema_view.with_ui('swagger')),
]
