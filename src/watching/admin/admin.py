from django.contrib.admin import register

from utils.admin import BaseModelAdmin as ModelAdmin
from watching.admin.inlines import VideoInline, ParticipantInline
from watching.models.models import *

__all__ = [
    'GenreAdmin',
    'PersonAdmin',
    'CategoryAdmin',
    'TvSeriesAdmin',
    'SeasonAdmin',
    'EpisodeAdmin',
    'FilmAdmin',
]


@register(Genre)
class GenreAdmin(ModelAdmin):
    list_display = ('id', 'title',)
    search_fields = ('title',)


@register(Person)
class PersonAdmin(ModelAdmin):
    list_display = ('id', 'first_name', 'last_name')
    search_fields = ('first_name', 'last_name')


@register(Category)
class CategoryAdmin(ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)


@register(TvSeries)
class TvSeriesAdmin(ModelAdmin):
    list_display = ('id', 'title')
    search_fields = ('title',)
    filter_horizontal = ('categories', 'genre_set')

    inlines = [
        ParticipantInline,
    ]


@register(Season)
class SeasonAdmin(ModelAdmin):
    list_display = ('id', '__str__')
    search_fields = ('title', 'tv_series__title')


@register(Episode)
class EpisodeAdmin(ModelAdmin):
    list_display = ('id', 'title', 'number')
    search_fields = ('title', 'season__title', 'season__tv_series__title')
    autocomplete_fields = ('season',)

    inlines = [
        VideoInline,
    ]


@register(Film)
class FilmAdmin(ModelAdmin):
    list_display = ('id', 'title')
    search_fields = ('title',)
    filter_horizontal = ('categories', 'genre_set')

    inlines = [
        VideoInline,
        ParticipantInline,
    ]
