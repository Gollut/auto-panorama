from django.contrib.admin import TabularInline

from watching.models import Video, Participant


class VideoInline(TabularInline):
    model = Video


class ParticipantInline(TabularInline):
    model = Participant
    autocomplete_fields = ('person',)
