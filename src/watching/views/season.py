from rest_framework.generics import ListAPIView

from utils.mixins import FilterQuerysetMixin
from watching import serializers
from watching.models import Season

__all__ = [
    'SeasonView',
]


class SeasonView(FilterQuerysetMixin, ListAPIView):
    queryset = Season.objects.all()
    serializer_class = serializers.SeasonSerializer

    filter_kwargs = {'tv_series_id': 'id'}
