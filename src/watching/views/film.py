from rest_framework.generics import ListAPIView, RetrieveAPIView

from watching import serializers
from watching.models import Film

__all__ = [
    'FilmView',
    'FilmDetailView',
]


class FilmView(ListAPIView):
    queryset = Film.objects.all()
    serializer_class = serializers.FilmSerializer


class FilmDetailView(RetrieveAPIView):
    queryset = Film.objects.all()
    serializer_class = serializers.FilmDetailSerializer

    lookup_url_kwarg = 'id'
