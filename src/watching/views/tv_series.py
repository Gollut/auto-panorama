from rest_framework.generics import ListAPIView

from watching import serializers
from watching.models import TvSeries

__all__ = [
    'TvSeresView',
]


class TvSeresView(ListAPIView):
    queryset = TvSeries.objects.all()
    serializer_class = serializers.TvSeriesSerializer
