from utils.tests.base_classes import BaseTestCase


class TestFilmView(BaseTestCase):
    VIEW = 'watching:films'
    LEN = 2

    def _arrange(self):
        for _ in range(self.LEN):
            self.watching_factory.get_film(is_set_participants=True, is_set_genres=True)

    def test_get(self):
        self._arrange()
        r = self.client.get(self.url)
        self.assert200(r)
        self.assertLenPaginatedResponse(r, self.LEN)


class TestFilmDetailView(BaseTestCase):
    VIEW = 'watching:films_detail'
    LEN = 2

    def setUp(self):
        super().setUp()
        self.film = self.watching_factory.get_film(is_set_participants=True, is_set_videos=True)

    def get_reverse_kwargs(self):
        return {'id': self.film.pk}

    def _arrange(self):
        for _ in range(self.LEN):
            pass

    def test_get(self):
        self._arrange()
        r = self.client.get(self.url)
        self.assert200(r)


class TestTvSeresView(BaseTestCase):
    VIEW = 'watching:tv_series'
    LEN = 2

    def _arrange(self):
        for _ in range(self.LEN):
            self.watching_factory.get_tv_series(is_set_participants=True)

    def test_get(self):
        self._arrange()
        r = self.client.get(self.url)
        self.assert200(r)
        self.assertLenPaginatedResponse(r, self.LEN)


class TestSeasonView(BaseTestCase):
    VIEW = 'watching:seasons'
    LEN = 2

    def setUp(self):
        super().setUp()
        self.tv_series = self.watching_factory.get_tv_series()

    def get_reverse_kwargs(self):
        return {'id': self.tv_series.pk}

    def _arrange(self):
        for _ in range(self.LEN):
            self.watching_factory.get_season(tv_series=self.tv_series)

    def test_get(self):
        self._arrange()
        r = self.client.get(self.url)
        self.assert200(r)
        self.assertLenPaginatedResponse(r, self.LEN)


class TestEpisodeView(BaseTestCase):
    VIEW = 'watching:episodes'
    LEN = 2

    def setUp(self):
        super().setUp()
        self.season = self.watching_factory.get_season()

    def get_reverse_kwargs(self):
        return {'id': self.season.tv_series_id, 'season_id': self.season.pk}

    def _arrange(self):
        for _ in range(self.LEN):
            self.watching_factory.get_episode(season=self.season)

    def test_get(self):
        self._arrange()
        r = self.client.get(self.url)
        self.assert200(r)
        self.assertLenPaginatedResponse(r, self.LEN)


class TestEpisodeDetailView(BaseTestCase):
    VIEW = 'watching:episodes_detail'

    def setUp(self):
        super().setUp()
        self.episode = self.watching_factory.get_episode(True, True)

    def get_reverse_kwargs(self):
        return {
            'id': self.episode.season.tv_series_id,
            'season_id': self.episode.season_id,
            'episode_id': self.episode.pk
        }

    def test_get(self):
        r = self.client.get(self.url)
        self.assert200(r)
