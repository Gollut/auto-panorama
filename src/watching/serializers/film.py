from rest_framework import serializers

from watching.models import Film
from watching.serializers.video import VideoSerializer

__all__ = [
    'FilmSerializer',
    'FilmDetailSerializer',
]


class FilmSerializer(serializers.ModelSerializer):
    genre_set = serializers.SlugRelatedField(many=True, read_only=True, slug_field='title')

    class Meta:
        model = Film
        fields = ('id', 'title', 'description', 'genre_set')
        read_only_fields = ('title', 'description')


class FilmDetailSerializer(serializers.ModelSerializer):
    videos = VideoSerializer(read_only=True, many=True)

    class Meta:
        model = Film
        fields = ('id', 'title', 'description', 'videos')
