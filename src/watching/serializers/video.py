from rest_framework import serializers

from watching.models import Video

__all__ = [
    'VideoSerializer',
]


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ('quality', 'file')
