from rest_framework import serializers

from watching.models import Season

__all__ = [
    'SeasonSerializer',
]


class SeasonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Season
        fields = ('id', 'title', 'description', 'number')
        read_only_fields = ('title', 'description', 'number')
