from rest_framework import serializers

from watching.models import Person

__all__ = [
    'PersonSerializer',
]


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ('id', 'first_name', 'last_name')
        read_only_fields = ('first_name', 'last_name')
