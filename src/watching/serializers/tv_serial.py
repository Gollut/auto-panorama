from rest_framework import serializers

from watching.models import TvSeries
from watching.serializers.participant import ParticipantSerializer

__all__ = [
    'TvSeriesSerializer',
]


class TvSeriesSerializer(serializers.ModelSerializer):
    participants = ParticipantSerializer(many=True, read_only=True)

    class Meta:
        model = TvSeries
        fields = ('id', 'title', 'description', 'participants')
        read_only_fields = ('title', 'description')
