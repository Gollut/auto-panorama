from rest_framework import serializers
from watching.serializers import video
from watching.models import Episode

__all__ = [
    'EpisodeSerializer',
    'EpisodeDetailSerializer',
]


class EpisodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Episode
        fields = ('id', 'title', 'description', 'number')
        read_only_fields = ('title', 'description', 'number')


class EpisodeDetailSerializer(serializers.ModelSerializer):
    videos = video.VideoSerializer(many=True, read_only=True)

    class Meta:
        model = Episode
        fields = ('id', 'title', 'description', 'number', 'videos')
        read_only_fields = ('title', 'description', 'number')
