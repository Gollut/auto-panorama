from rest_framework import serializers

from watching.models import Genre

__all__ = [
    'GenreSerializer',
]


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ('id', 'title')
        read_only_fields = ('title',)
