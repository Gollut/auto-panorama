from django.urls import path
from watching import views

app_name = 'watching'

urlpatterns = [
    path('films/', views.FilmView.as_view(), name='films'),
    path('films/<int:id>/', views.FilmDetailView.as_view(), name='films_detail'),
    path('tv-series/', views.TvSeresView.as_view(), name='tv_series'),
    path('tv-series/<int:id>/seasons/', views.SeasonView.as_view(), name='seasons'),
    path('tv-series/<int:id>/seasons/<int:season_id>/episodes/', views.EpisodeView.as_view(), name='episodes'),
    path('tv-series/<int:id>/seasons/<int:season_id>/episodes/<int:episode_id>/', views.EpisodeDetailView.as_view(),
         name='episodes_detail'),

]
