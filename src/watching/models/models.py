from django.db import models
from django.db.models import CASCADE
from django.utils.functional import cached_property

from utils.models import TitledModelMixin, DescribedModelMixin, NumberedModelMixin, ReleaseDateModelMixin, \
    MaturityRatingModelMixin, ImagedModelMixin
from utils.upload_to import UploadToFactory
from watching.choices import RoleType, QualityType
from watching.models import Genred, Participle, Viewable, Categorized, Rated
from django.utils.translation import gettext_lazy as _

__all__ = [
    'Genre',
    'Film',
    'PromoFilm',
    'TvSeries',
    'Season',
    'Episode',
    'Person',
    'Participant',
    'Video',
    'Category',
]


class Genre(TitledModelMixin):
    class Meta:
        app_label = 'watching'
        verbose_name = _('genre')
        verbose_name_plural = _('genres')

    def __str__(self):
        return self.title


class TvSeries(TitledModelMixin,
               DescribedModelMixin,
               MaturityRatingModelMixin,
               ImagedModelMixin,
               Rated,
               Categorized,
               Participle,
               Genred):
    id = models.AutoField(primary_key=True)
    genred_ptr = models.OneToOneField('Genred', CASCADE, parent_link=True, related_name='tv_series_link')
    participle_ptr = models.OneToOneField('Participle', CASCADE, parent_link=True, related_name='tv_series_link')
    categorized_ptr = models.OneToOneField('Categorized', CASCADE, parent_link=True, related_name='tv_series_link')
    rated_ptr = models.OneToOneField('Rated', CASCADE, parent_link=True, related_name='tv_series_link')

    date_start = models.DateField()
    date_end = models.DateField(null=True, blank=True)

    class Meta:
        app_label = 'watching'
        verbose_name = _('tv series')
        verbose_name_plural = _('tv series')

    def __str__(self):
        return self.title


class Season(NumberedModelMixin,
             Rated,
             Participle):
    id = models.AutoField(primary_key=True)
    title = models.CharField(_('title'), max_length=32, blank=True)
    description = models.TextField(_('description'), blank=True)
    tv_series = models.ForeignKey('TvSeries', CASCADE, related_name='seasons')
    participle_ptr = models.OneToOneField('Participle', CASCADE, parent_link=True, related_name='season_link')
    rated_ptr = models.OneToOneField('Rated', CASCADE, parent_link=True, related_name='season_link')

    class Meta:
        app_label = 'watching'
        verbose_name = _('season')
        verbose_name_plural = _('seasons')

    def __str__(self):
        return f'{self.tv_series.title}, {self.number}'


class Episode(DescribedModelMixin,
              NumberedModelMixin,
              ReleaseDateModelMixin,
              Rated,
              Viewable,
              Participle):
    id = models.AutoField(primary_key=True)
    title = models.CharField(_('title'), max_length=32, blank=True)
    season = models.ForeignKey('Season', CASCADE, related_name='episodes')
    participle_ptr = models.OneToOneField('Participle', CASCADE, parent_link=True, related_name='episode_link')
    viewable_ptr = models.OneToOneField('Viewable', CASCADE, parent_link=True, related_name='episode_link')
    rated_ptr = models.OneToOneField('Rated', CASCADE, parent_link=True, related_name='episode_link')

    class Meta:
        app_label = 'watching'
        verbose_name = _('episode')
        verbose_name_plural = _('episodes')

    def __str__(self):
        return f'{self.season.tv_series.title}, {self.season.number}, {self.number}'


class Film(TitledModelMixin,
           DescribedModelMixin,
           ReleaseDateModelMixin,
           MaturityRatingModelMixin,
           ImagedModelMixin,
           Rated,
           Categorized,
           Participle,
           Viewable,
           Genred):
    id = models.AutoField(primary_key=True)
    genred_ptr = models.OneToOneField('Genred', CASCADE, parent_link=True, related_name='film_link')
    participle_ptr = models.OneToOneField('Participle', CASCADE, parent_link=True, related_name='film_link')
    viewable_ptr = models.OneToOneField('Viewable', CASCADE, parent_link=True, related_name='film_link')
    rated_ptr = models.OneToOneField('Rated', CASCADE, parent_link=True, related_name='film_link')
    categorized_ptr = models.OneToOneField('Categorized', CASCADE, parent_link=True, related_name='film_link')

    class Meta:
        app_label = 'watching'
        verbose_name = _('film')
        verbose_name_plural = _('films')

    def __str__(self):
        return self.title


class PromoFilm(models.Model):
    film = models.OneToOneField('Film', CASCADE, primary_key=True)
    banner = models.ImageField(upload_to=UploadToFactory('watching/banner_film/banner'))
    order = models.PositiveSmallIntegerField()

    class Meta:
        app_label = 'watching'
        verbose_name = _('promo film')
        verbose_name_plural = _('promo films')

    def __str__(self):
        return self.film.title


class Person(models.Model):
    first_name = models.CharField(max_length=16)
    last_name = models.CharField(max_length=32)

    class Meta:
        app_label = 'watching'
        verbose_name = _('person')
        verbose_name_plural = _('persons')

    @cached_property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return self.full_name


class Participant(models.Model):
    participle = models.ForeignKey('Participle', CASCADE, related_name='participants')
    person = models.ForeignKey('Person', CASCADE, related_name='participants')
    role = models.CharField(max_length=2, choices=RoleType.choices())

    class Meta:
        app_label = 'watching'
        verbose_name = _('participant')
        verbose_name_plural = _('participants')

    def __str__(self):
        return ''


class Video(models.Model):
    viewable = models.ForeignKey('Viewable', CASCADE, related_name='videos')
    quality = models.CharField(max_length=4, choices=QualityType.choices())
    file = models.FileField(upload_to=UploadToFactory('watching/video/file'))

    class Meta:
        app_label = 'watching'
        verbose_name = _('video')
        verbose_name_plural = _('videos')

    def __str__(self):
        return ''


class Category(TitledModelMixin):
    class Meta:
        app_label = 'watching'
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def __str__(self):
        return self.title
