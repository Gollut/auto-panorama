from django.db import models

from utils.upload_to import UploadToFactory

__all__ = [
    'Genred',
    'Participle',
    'Viewable',
    'Rated',
    'Categorized',
]


class Genred(models.Model):
    genred_id = models.AutoField(primary_key=True)

    genre_set = models.ManyToManyField('Genre', related_name='genred_set')

    class Meta:
        app_label = 'watching'


class Participle(models.Model):
    participle_id = models.AutoField(primary_key=True)

    class Meta:
        app_label = 'watching'


class Viewable(models.Model):
    viewable_id = models.AutoField(primary_key=True)
    duration = models.DurationField()
    preview = models.ImageField(upload_to=UploadToFactory('watching/viewable/preview'))

    class Meta:
        app_label = 'watching'


class Rated(models.Model):
    rated_id = models.AutoField(primary_key=True)

    class Meta:
        app_label = 'watching'


class Categorized(models.Model):
    categorized_id = models.AutoField(primary_key=True)

    categories = models.ManyToManyField('Category', related_name='categorized_set')

    class Meta:
        app_label = 'watching'
