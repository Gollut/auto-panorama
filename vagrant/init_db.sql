CREATE DATABASE auto_panorama;
CREATE USER auto_panorama_user WITH PASSWORD 'password123';
ALTER ROLE auto_panorama_user SET client_encoding TO 'utf8';
ALTER ROLE auto_panorama_user SET default_transaction_isolation TO 'read committed';
ALTER ROLE auto_panorama_user SET timezone TO 'UTC';
GRANT ALL ON DATABASE auto_panorama TO auto_panorama_user;
ALTER USER auto_panorama_user CREATEDB;
